class ApiService

  include HTTParty

  base_uri "json.platinumelectionresults.com"

  class << self

    def jurisdictions
      request("jurisdictions")
    end

    def district_types(jurisdiction_id)
      request("district_types", { query: {"jurisdiction_id" => jurisdiction_id} })
    end

    def townships(jurisdiction_id)
      request("townships", { query: {"jurisdiction_id" => jurisdiction_id} })
    end

    def precincts(jurisdiction_id, township_id)
      request("precincts", { query: {"jurisdiction_id" => jurisdiction_id, "township_id" => township_id}})
    end

    def races(jurisdiction_id, district_id)
      request("races", { query: {"jurisdiction_id" => jurisdiction_id, "district_type_id" => district_id}})
    end

    def race_precincts(jurisdiction_id, race_id)
      request("race_precincts", { query: {"jurisdiction_id" => jurisdiction_id, "race_no" => race_id}})
    end

    def turnout(location ,jurisdiction_id)
      request("turnouts/#{location}", { query: {"jurisdiction_id" => jurisdiction_id} })
    end

    def request(endpoint, options = {})
      response = get("/#{endpoint}.json", options)
      if response.success?
        response.parsed_response
      else
        raise response.response
      end
    end
  end
end