class Race
  include ActiveModel::Model

  attr_accessor :id, :name, :vote_for, :precincts, :district_name, :candidates

  class << self
    def results(jurisdiction_id, id)
      response = ApiService.race_precincts(jurisdiction_id, id)
      self.new(self.mapper(response))
    end

    def find_id(jurisdiction_id, district_id)
      response = ApiService.races(jurisdiction_id, district_id)
      result = response["district_type"]["races"].find { |hash| hash["race_party"] == "DEM" }
      result["race_sequence"]
    end

    def mapper(result)
      {
        id:               result["race"]["race_no"],
        name:             result["race"]["race_name"],
        vote_for:         result["race"]["vote_for"].humanize,
        district_name:    result["race"]["district_name"],
        precincts:        result["race"]["precincts"],
        candidates:       result["race"]["precincts"][0]["candidates"].map {|candidate| candidate["name"]}
      }
    end
  end
end