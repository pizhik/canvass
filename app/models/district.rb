class District
  include ActiveModel::Model

  attr_accessor :id, :name

  class << self

    def all(jurisdiction_id)
      response = ApiService.district_types(jurisdiction_id)
      response.map { |hash| self.new(hash) }
    end
  end
end