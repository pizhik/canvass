class Candidate
  include ActiveModel::Model

  attr_accessor :id, :name, :votes, :party
end