class Jurisdiction
  include ActiveModel::Model

  attr_accessor :id, :name, :phone, :email, :clerk_title, :election_name, :election_date, :status_string, :county_seal, :county_sig, :has_township, :is_primary, :upload_type, :election_id

  def initialize(hash)
    super
    temp_arr = self.election_name.split(/,\s/)
    @election_date = "#{temp_arr[0]}, #{temp_arr[1]}" 
    @election_name = "#{temp_arr[2]}"
  end

  class << self

    def all
      response = ApiService.jurisdictions
      response.map { |hash| self.new(hash) }
    end

    def find(id)
      response = ApiService.jurisdictions
      result = response.find { |hash| hash["id"] == id }
      self.new(result)
    end
  end
end