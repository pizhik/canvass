class Report
  require "prawn"
  require "open-uri"

  def initialize()
    @pdf = Prawn::Document.new
  end

  def title_page(jurisdiction, district_name)
    @pdf.bounding_box([35, 686], :width => 470, :height => 650) do
      @pdf.stroke_bounds
      @pdf.move_down 40
      @pdf.font_size 25
      @pdf.text "#{jurisdiction.election_name}\n#{jurisdiction.election_date}\nOfficial Democratic Canvass\n" + 
        "#{jurisdiction.name} County, Illinois", :align => :center

      @pdf.move_down 35
      @pdf.image open("http://illinois.platinumelectionresults.com/assets/sigs_seals/#{jurisdiction.id}_seal.jpg"), 
        :position => :center, :width => 145, :height => 145

      @pdf.move_down 45
      @pdf.font_size 20
      @pdf.text district_name, :align => :center

      @pdf.move_down 90
      @pdf.font_size 12
      @pdf.text "Dated: #{jurisdiction.election_date}\nIssued by:\n", :indent_paragraphs => 38
      @pdf.image open("http://illinois.platinumelectionresults.com/assets/sigs_seals/#{jurisdiction.id}_sig.jpg"),
        :position => 38, :width => 125, :height => 41
      @pdf.text jurisdiction.clerk_title.gsub(", ", "\n"), :indent_paragraphs => 38
    end
  end

  def results_page(jurisdiction, race, turnouts)
    @pdf.font_size 10
    y_position = @pdf.cursor
    @pdf.bounding_box([50, y_position], :width => 150, :height => 70) do
      @pdf.text_box "#{jurisdiction.election_name}\n#{jurisdiction.election_date}\n#{jurisdiction.clerk_title.gsub(", ", "\n")}", :align => :center
    end

    @pdf.bounding_box([170, y_position], :width => 350, :height => 70) do
      @pdf.text_box "#{race.district_name.upcase}\n#{race.name.upcase}\n(Vote for #{race.vote_for})", :align => :center
    end

    candidates = race.candidates

    results = race.precincts.map do |precinct|
      turnout = turnouts.find {|p| p["name"] == precinct["precinct_name"].upcase}
      votes = precinct["candidates"].map do |candidate|
        candidate["votes"]
      end
      [precinct["precinct_name"].upcase, precinct["registered_voters"], precinct["total_ballots_cast"], "#{turnout["dem_turnout"]}%"] + votes
    end
    data = [["", "REGISTERED VOTES", "BALLOTS CAST", "TURNOUT"] + candidates] + results


    @pdf.table(data, :header => true, :cell_style => { :borders => [:bottom] }) do
      row(0).height = 120
      row(0).width = 35
      row(0).overflow = :expand
      row(0).rotate = 90
      row(0).borders = [:bottom]
      column(0).width = 150
    end
  end

  def last_page(jurisdiction, district_name)
    @pdf.font "Helvetica", :style => :bold
    @pdf.text "CANVASS OF VOTES\nin #{jurisdiction.name} County, Illinois at the #{jurisdiction.election_name} on #{jurisdiction.election_date}", :align => :center
    @pdf.line_width = 1.5
    @pdf.stroke do
      @pdf.horizontal_rule
    end

    @pdf.move_down 10
    @pdf.font "Helvetica"
    @pdf.text "STATE OF ILLINOIS\nCOUNTY OF #{jurisdiction.name.upcase}"

    @pdf.move_down 15
    @pdf.font "Helvetica", :style => :bold
    @pdf.text district_name, :align => :center
    @pdf.move_down 15
    @pdf.font "Helvetica"
    @pdf.font_size 11
    @pdf.text "I, #{jurisdiction.clerk_title}, do hereby certify that the above is a correct copy of the Canvass of votes cast " +
         "at the #{jurisdiction.election_name} held in #{jurisdiction.name} County on #{jurisdiction.election_date}. This canvass was made by the County " +
         "Canvassing Board of #{jurisdiction.name} County and is now on file in my office."
    @pdf.move_down 55

    @pdf.text "Dated: #{jurisdiction.election_date}", :indent_paragraphs => 90
    @pdf.image open("http://illinois.platinumelectionresults.com/assets/sigs_seals/#{jurisdiction.id}_sig.jpg"),
        :at => [280, 540], :width => 125, :height => 41
    @pdf.line [240, 505], [450, 505]
    @pdf.stroke
    @pdf.font_size 8
    @pdf.move_down 5
    @pdf.text_box jurisdiction.clerk_title, :at => [280, 500]
  end

  def new_page
    @pdf.start_new_page
  end

  def save_file
    @pdf.render
  end
end