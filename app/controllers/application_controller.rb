class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
    #@jurisdictions = Jurisdiction.all
    @jurisdictions = [Jurisdiction.find(6)]
  end

  def print_report
    jurisdiction = Jurisdiction.find(params[:id].to_i)
    race_id = Race.find_id(jurisdiction.id, 1)
    race = Race.results(jurisdiction.id, race_id)
    turnouts = ApiService.turnout("precincts", jurisdiction.id)["precincts"]
    respond_to do |format|
      format.pdf do
        report = Report.new()
        report.title_page(jurisdiction, race.district_name.upcase)
        report.new_page
        report.results_page(jurisdiction, race, turnouts)
        report.new_page
        report.last_page(jurisdiction, race.district_name.upcase)
        send_data report.save_file, filename: "#{jurisdiction.name}_canvass.pdf", type: "application/pdf", disposition: "attachment"
      end
    end
  end
end
